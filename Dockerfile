FROM fingerprintsoft/maven
MAINTAINER Fuzail Sarang <fuzail@fingerprintsoft.co>

ENV PJS_HOME=/usr/lib/phantomjs

RUN cd /tmp && \
  apt-get update && \
  apt-get install build-essential chrpath libssl-dev libxft-dev -y && \
  apt-get install libfreetype6 libfreetype6-dev -y && \
  apt-get install libfontconfig1 libfontconfig1-dev -y && \
  cd /tmp && \
  export PHANTOM_JS="phantomjs-2.1.1-linux-x86_64" && \
  wget https://github.com/Medium/phantomjs/releases/download/v2.1.1/$PHANTOM_JS.tar.bz2 && \
  tar xvjf $PHANTOM_JS.tar.bz2 && \
  mv $PHANTOM_JS /usr/local/share && \
  ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin && \
  rm -rf /tmp/*

CMD ["phantomjs", "--version"]

