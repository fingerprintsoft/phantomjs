MAJOR ?= 2
MINOR ?= 1
BUILD ?= 1
.PHONY: all

all: build push

build:
	docker build --rm -t fingerprintsoft/phantomjs .
	docker tag fingerprintsoft/phantomjs fingerprintsoft/phantomjs:${MAJOR}.${MINOR}.${BUILD}

push:
	docker push fingerprintsoft/phantomjs
	docker push fingerprintsoft/phantomjs:${MAJOR}.${MINOR}.${BUILD}
